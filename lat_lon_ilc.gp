# Plots the ilc coefficients as functions of glat and glon.

set xlabel "GLAT"
set ylabel "GLON"
set zlabel "ILC"

splot "lat_lon_ilc.data" using 1:2:4 with points palette pointsize 2 pointtype 7
pause -1
