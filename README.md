# P2 Project (Duque, El-Achkar)

> **Comment on hosting data in the git repo**

> There is some data (images) in the git repo (in the output_* folders) because
> some of us worked offline on the data, which was thus accessed once and for all
> through the git repo. The large 2048 data was of course not pushed, only the 
> 256 patches, which represented in total under 1GB of data.

## Smoothing and units

We only use bands >= 100 Ghz.

Smoothing from a to b fwhm is sqrt(a**2 - b**2), the fwhm of different bands
come from bib[2], table 3. We smooth because the psf is different (different
lambdas/D)  and  ILC requires adding maps together.

Units convert from MJy/sr to K_CMB with conversion in bib[3], table 6.
Raw 2048 -> smoothing(..., sqrt(...)) -> units -> saved in WORKDIR/data_2048

- 256 px = 7.33 deg
- beam = 9.66 arcmin ~ 6 px

## Patching

We read galactic coords of GC from the catalog files retreived from the SZ
cluster database. We load them with the function: get_fits_coords(fname).
We use gnomview to get the 2D array which is composed from the pixels around
the selected coordinates and we create smaller maps (patches) which We Will use
to do the ILC.

The patches are 256x256, and are smoothed and renormalized
(units) because they were made from the smoothed and normalized 2048x2048
all sky images.

Each output_i directory for integer i contain the clusters from catalog i.
patches.

## Tests

Generally a cluster is supposed to be brighter in high frequencies(because SZ shifts
the spectrum to the right). While doing
our patches we used this to check that the clusters were more visible when we
looked at the maps with higher frequencies

### Null test

To verify that our codes are working we look at an empty region in the sky
and check that the ILC/MILCA processed patches do not show a significant signal at the
center of the patch.

Note: We noticed that some regions in the SZ NULL patches have negative values.
These are in fact sources that are brighter at low frequencies like radio sources
or infrared emissions. These sources get brighter when we look at the lower
frequency maps. We thought that we can estimate an approximate order of magnitude
of the sky-density number of these sources in the sky (since the NULL patches
are in fact randomly distributed in the sky).

## ILC Method

ILC method calculates an astrophysical component as a linear combination of all
the bands and
chooses the combination coefficients so as to minimize the variance of the
rebuilt map.

In the case of the CMB, the bands are normalized such that the flux is the same
in each band, which allows to obtain the explicit formula for CMB (12 in
bib[1])

In the case of the tSZ, the spectra is found at bib[3] table 6.
In linear combination, beware that the units of fluxes depend on the band
(see header of fits for info) and that the PSF varies as well.

## Multiple Component ILC

The ILC method can be improved by providing the spectrum of another component
which we do not seek to reconstruct but which improves the reconstruction of
the aimed component. For example, one can provide the CMB (flat), tSZ
(according to bib[3]) and the IR component (BB). With these additional
spectra, the number of components is 3, thus N_c > 1 + N_rc and the results are
improved.

In the case where N_rc = 1,
N_c = 1 + N_rc and we find that the results are the same (simple ILC versus
MCILC), in accordance with the maths of the method.


## MILCA Method

In order to give a scale-sensitive dimension to the ILC method (which works
pixel by pixel), the MILCA method applies the ILC method to the scale-enhanced
versions of the images, ie the images after having passed by filters which
recognize structures of different scales. In our case they are gaussian
differences filters. After, the ILC results of each filtered image are
recombined to produce the MILCA result.


## Discussion

Cluster from article:...

Detection efficiency (SNR ratio)...

MILCA coefficient against latitude study...

Result on double clusters, interaction clusters...

NULL TEST for MILCA on same patches as ILC NULL TEST.

## Notes:

- ILC first attempt:We used the method on images that we reduced to a
resolution of 512.
We did the first try without adding correcting the FEP and the units.
The results didn't seem wrong at first because:
1) ILC will minimize the variance
2) by reducing the resolution to 512 we might have taken a beaming that is
greater than the PSF of the telescope.
So because we are taking patches we don't need to reduce the resolution.

- optimization and execution time problems

- noise: the noise from each map is added (not same beam and units)

- to verify that its SZ ...

## Nomenclature

- `./data_2048`: all sky smoothed and unitized
- `./output_*`: 256 patches and ILC/MILCA output images
- `./szdb*.data`: ILC, MCILC and MILCA result files
- `./szdb*.fits`: cluster catalogs in fits format 
- `./NULL_Test`: MILCA/ILC results and patches on random sky parcels
- `./filters`: the 2D filters used for the convolution in the MILCA method

#### Bib

[1]: MILCA, a MODIFIED ..., Hurier, Macias-Perez, Hildebrandt (arXiv:1007.1149)

[2]: Planck 2013 results VII... (arXiv:1303.5068)

[3]: Planck 2013 results IX... (arXiv:1303.5070)
