'''Test file to try ILC on the allsky images.'''

import healpy as hp
import glob
import numpy as np
from helpers import *

UNITS = [1, 1, 1, 1, 58.04, 2.27] #pour remettre tout en Kelvin CMB
FWHM_arcmin = np.array([9.66, 7.27, 5.01, 4.86, 4.84, 4.63])  #FWHM en arcmin
FWHM = FWHM_arcmin * 2 * np.pi/21600 #FHWM en radians
FWHM_correction = [np.sqrt(FWHM[0]**2 - f**2) for f in FWHM] #FHWM corrigee

for j in range (2):
    images = [WORK_DIR + 'amas' + str(j)+ '/' + 'amas' + str(j)+ '_' + band + '.fits' for band in BANDS]
    print(images)

    for i, file_name in enumerate(images):
        a = hp.sphtfunc.smoothing(hp.read_map(file_name), FWHM_correction[i])
        a = a * UNITS[i]
        hp.write_map(WORK_DIR + "smooth_" + file_name.split('/')[-1], a)
