'''Helper functions for the MILCA project.'''

import astropy.io.fits as fits
import numpy as np
import matplotlib.pyplot as plt
import getpass
import glob

# Functions
def get_fits_coords(fname):
    """Return the data  dictionary from SZ catalogue.

    The usefull keys of the returned dictionary are:
        'GLAT', 'GLON', RA', 'DEC', 'NAME', 'PSZ1_SNR'."""
    with fits.open(fname) as fits_file:
        return fits_file[1].data

def fits_to_png(fname):
    """Write the fits image map to a png file."""
    with fits.open(fname) as fits_file:
        plt.figure(1)
        try:
            plt.imshow(fits_file[0].data)
        except TypeError:
            plt.imshow(fits_file[1].data)
        plt.savefig(fname.split('/')[-1][-5:] + '.png')

def array_to_fits(array, fname):
    """Write a 2D array to a given fits file."""
    hdu = fits.ImageHDU(array)
    hdu.writeto(fname, overwrite=True)

def ILC(spectrum, array_of_arrays):
    """Return the ILC transform of all images in array_of_array with spectrum."""
    T = np.array([array.flatten() for array in array_of_arrays])
    CT = np.cov(T)

    CT_inv = np.linalg.inv(CT)
    S_ILC = np.dot(spectrum, np.dot(CT_inv, T))/(np.dot(spectrum, np.dot(CT_inv, spectrum)))
    coefs = np.dot(CT_inv, spectrum)/(np.dot(spectrum, np.dot(CT_inv, spectrum)))
    ret = np.array([[S_ILC[256 * j + i] for i in range(256)] for j in range(256)])
    return ret, coefs

def fits_to_array(fname):
    """Returns the 2D array of fits file."""
    with fits.open(fname) as fits_file:
        return fits_file[1].data

#Symmetry functions and radius
s = lambda x: x if x < 128 else 256 - x
r = lambda i, j: np.sqrt(s(i)**2 + s(j)**2)
mod = lambda x: x if x < 180.0 else (x - 360.0)

def fourier_gaussian(W):
    """Returns a gaussians of given width for the filters.

    This is to be used with np.fft/ifft, hence the filter is given on (0, T)
    and not on (-T/2, T/2), hence the non-centered appearance."""
    if W == 256:
        return np.array([[1 for i in range(256)] for j in range(256)])
    if W == 0:
        return np.array([[0 for i in range(256)] for j in range(256)])
    else:
        return np.array([[np.exp( - r(i, j) ** 2 / (2 * W** 2)) for i in range(256)] for j in range(256)])

def snr(array):
    """Returns the signal to noise ratio of array.

    The SNR is ms - m /sig, where:
        * ms is the mean over pixels where a > max - sig
        * m is the mean
        * sig is the std deviation of the image
    """
    sig = np.std(array)
    m = array.mean()
    max_a = np.max(array)
    A1 = np.where(array > max_a - sig, array, np.nan)
    a = np.nanmean(A1)
    ret = (a - m)/sig
    if np.isnan(ret):
        return 0.
    else:
        return ret

def filter_array(fil, array):
    """Filter array in Fourier space."""
    return np.fft.ifft2(fil * np.fft.fft2(array))

def list_to_string(l):
    """String rep of list for output."""
    ret = ''
    for a in l:
        ret = ret + str(a) + ' '
    return ret

# Globals
DATA_DIR = "/home/abeelen/Planck/maps/"
WORK_DIR = "/home/%s/P2_hel-achk_rduque/" % getpass.getuser()
BANDS = ['100', '143', '217', '353', '545', '857']
WIDTHS = [0, 1, 2, 4, 8, 12, 16, 20, 25, 32, 256]
N_BANDS = len(BANDS)
N_WIDTHS = len(WIDTHS)
N_FILTERS = N_WIDTHS - 1
N_PIX = 256**2

#Spectra
SZ_spectrum = np.array([-0.2485, -0.35923, 5.152, 0.161098, 0.06918, 0.0380])
CMB_spectrum = np.array([1., 1., 1., 1., 1., 1.])
MC_spectrum = np.array([[-0.2485, 1],
              [-0.35923, 1],
              [5.152, 1],
              [0.161098, 1],
              [0.06918, 1],
              [0.0380, 1]])

# Catalogue dependent variables
CATALOGUE = 'szdb_table_8.fits'
ILC_FILE = 'szdb_ilc_256.data'
MILCA_FILE = 'szdb_milca_256.data'
SNR_STR = 'PSZ1_SNR'
OUTPUT_DIR = WORK_DIR + 'output_256/'

dic = get_fits_coords(CATALOGUE)
N_CLUSTERS = len(dic['NAME'])

# WARNING: changing names in cluster dictionary
for i in range(N_CLUSTERS):
    dic['NAME'][i] = dic['NAME'][i].split(' ')[-1]
