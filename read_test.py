'''Get-your-hands-inside script for healpy.'''

import healpy as hp
import glob

# Globals
DATA_DIR = "/home/abeelen/Planck/maps/"
WORK_DIR = "/home/rduque/P2_hel-achk_rduque/"

def mplot(name):
	array = hp.read_map(DATA_DIR + name, )
	hp.mollview(array)

#array = hp.read_map(DATA_DIR + "HFI_SkyMap_857_2048_R2.00_halfmission-2.fits")
#degraded = hp.pixelfunc.ud_grade(array, nside_out = 512)

factor = 4
for file_name in glob.glob(DATA_DIR + "*.fits"):
	print(file_name)
	l_array = hp.read_map(file_name)
	l_deg = hp.pixelfunc.ud_grade(l_array, nside_out = 512)
	hp.write_map(WORK_DIR + "red_" + file_name.split("/")[-1], l_deg)

#mplot("HFI_SkyMap_857_2048_R2.00_halfmission-2.fits")
