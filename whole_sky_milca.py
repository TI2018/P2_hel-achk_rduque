'''Applies the MILCA method to patches covering the whole sky.'''

import glob
import numpy as np
from helpers import *
import healpy as hp

sky_folder = WORK_DIR + 'output_whole_sky/'
filters = glob.glob(WORK_DIR + 'filters/*.fits')
filter_arr = np.array([fits_to_array(image) for image in filters])

#output file
milca_file = open(WORK_DIR + 'whole_sky.data', 'w')
milca_file.write("#GLAT GLON MILCA_SNR\n")

# all 2048 maps
sky_maps = [hp.read_map(WORK_DIR + 'data_2048/smooth_HFI_SkyMap_' + band + '_2048_R2.00_full.fits') for band in BANDS]

#patch the whole sky equally and apply milca
for i in range(-12, 13):
    lat = 7.33 * i
    for j in range(int(49 * np.cos(lat * np.pi / 180.0)) + 1):
        lon = j * 7.33 / np.cos(lat * np.pi / 180.0)
        print(str(lat), str(lon))

        #Make patches and apply milca
        patches = [hp.visufunc.gnomview(mapp, rot = (lon, lat),
                                     xsize = 256, ysize = 256, coord = 'G',
                                     return_projected_map = True) for mapp in sky_maps]
        matrix = np.array([[np.real(filter_array(fil, arr)) for fil in filter_arr] for arr in patches])
        MILCA_array = sum(np.array([ILC(SZ_spectrum, matrix[:, k])[0] for k in range(N_FILTERS)]))
        patch_snr = snr(MILCA_array)
        if patch_snr > 3:
            array_to_fits(MILCA_array, sky_folder + str(mod(lon)) + str(lat) + '.fits')
            milca_file.write(str(lat) + ' ' + str(mod(lon)) + ' ' + str(patch_snr) + '\n')

milca_file.close()
