'''Applies the MILCA method to the patches found in the output_* dirs.'''

import glob
import numpy as np
from helpers import *

filters = glob.glob(WORK_DIR + 'filters/*.fits')
filter_arr = np.array([fits_to_array(image) for image in filters])

#output file
milca_file = open(WORK_DIR + MILCA_FILE, 'w')
milca_file.write("#GLAT GLON 100 143 217 353 545 857 %s MILCA_SNR\n" % SNR_STR)

## Combination across filters
for i in range(N_CLUSTERS):
    folder = OUTPUT_DIR + dic['NAME'][i] + '/'
    print(folder)
    images = [folder + band + '.fits' for band in BANDS]
    images_arr = np.array([fits_to_array(image) for image in images])

    #array offiltered images.
    matrix = np.array([[np.real(filter_array(fil, arr)) for fil in filter_arr] for arr in images_arr])
    ILC_array = list()
    ILC_coeffs = list()

    #Apply ILC to filtered images.
    for j in range(N_FILTERS):
        current_ILC = ILC(SZ_spectrum, matrix[:, j])
        ILC_array.append(current_ILC[0])
        ILC_coeffs.append(current_ILC[1])

    #Combine imagesandweights.
    MILCA_array = sum(ILC_array)
    MILCA_coeffs = sum(ILC_coeffs)/N_FILTERS
    array_to_fits(MILCA_array, folder +  'MILCA_tSZ.fits')

    milca_file.write(str(dic['GLAT'][i]) + ' ' + str(mod(dic['GLON'][i])) + ' '\
        + list_to_string(MILCA_coeffs) + ' ' + str(dic[SNR_STR][i]) + ' ' + str(snr(MILCA_array, 5)) + '\n')

milca_file.close()
