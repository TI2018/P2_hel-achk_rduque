"""Trash."""

def unfilter_array(fil, array):
    return np.fft.ifft2(np.fft.fft2(array / fil))

def sz_spectrum(nu):
    '''tSZ spectrum for nu given in GHz.'''
    x = 13.766*nu*(10**(-6))
    return x*(np.exp(x) + 1)/(np.exp(x) - 1) - 4

def real_filter(W):
    """Returns a 16x16 real-space filter of width W.

    It can be used eg for convolution."""
    B1 = np.array([[np.exp(- W ** 2 * ((i - 8)**2 + (j - 8)**2)) \
                for i in range(16)] for j in range(16)])
    B2 = np.array([[np.exp(- (W + 1) ** 2 * ((i - 8)**2 + (j - 8)**2)) \
                for i in range(16)] for j in range(16)])
    return B1 - B2
