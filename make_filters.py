"""Write the MILCA filters to fits files."""

from helpers import *

for i in range(N_WIDTHS - 1):
# The filters are the differences of the gaussians.
    array_to_fits(- fourier_gaussian(WIDTHS[i]) + fourier_gaussian(WIDTHS[i+1]),
                WORK_DIR + 'filters/MILCA_filter_' + str(WIDTHS[i]) + '.fits')
