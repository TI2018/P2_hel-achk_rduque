'''This opens the smoothed all-sky 2048 fits and generates patches.

The patches are around clusters of which the coordinates are read from the
catalogue szdb_table_*.fits files.
'''

import healpy as hp
import numpy as np
import helpers
from helpers import *
import os

print("Processing %s." % CATALOGUE)
# Make folderto hold patches.
for i in range(N_CLUSTERS):
    os.system('mkdir -p %s%s' % (OUTPUT_DIR, dic['NAME'][i]))

#gnomview and save patches, call dic for coordinates.
for band in BANDS:
    mapp = hp.read_map(WORK_DIR + 'data_2048/smooth_HFI_SkyMap_' + band + '_2048_R2.00_full.fits')
    for i in range(N_CLUSTERS):
        # Create a directory per cluster and put the pathces (one per band) in there.
        print(dic['NAME'][i] + '/' + band)
        patch = hp.visufunc.gnomview(mapp, rot = (dic['GLON'][i], dic['GLAT'][i]),
                                     xsize = 256, ysize = 256, coord = 'G',
                                     return_projected_map = True)
        array_to_fits(patch.filled(np.nan), OUTPUT_DIR + dic['NAME'][i] + '/' + band + '.fits')
