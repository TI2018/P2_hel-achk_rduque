'''Applies the ILC method to the patches found in the ouput_* dirs.'''

import numpy as np
from helpers import *

#output file
ilc_file = open(WORK_DIR + ILC_FILE, 'w')
ilc_file.write("#GLAT GLON 100 143 217 353 545 857 %s ILC_SNR\n" % SNR_STR)
for i in range(N_CLUSTERS):
    folder = OUTPUT_DIR  + dic['NAME'][i] + '/'
    print(folder)
    arrays = np.array([fits_to_array(folder + band + '.fits') for band in BANDS])
    cal = ILC(SZ_spectrum, arrays)
    array_to_fits(cal[0], folder + 'ILC_tSZ.fits')
    ilc_file.write(str(dic['GLAT'][i]) + ' ' + str(mod(dic['GLON'][i])) + ' '\
        + list_to_string(cal[1]) + ' ' + str(dic[SNR_STR][i]) + ' ' + str(snr(cal[0], 5)) + '\n')

ilc_file.close()
