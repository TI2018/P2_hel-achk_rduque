'''Smooths the allsky 2048 images to resolution of least band and adapt units.'''

import healpy as hp
import numpy as np

# Globals
DATA_DIR = "/home/abeelen/Planck/maps/"
WORK_DIR = "/home/rduque/P2_hel-achk_rduque/"

BANDS = ['100', '143', '217', '353', '545', '857']
UNITS = [1, 1, 1, 1, 58.04, 2.27] #pour remettre tout en Kelvin CMB
FWHM_arcmin = np.array([9.66, 7.27, 5.01, 4.86, 4.84, 4.63])  #FWHM en arcmin
FWHM = FWHM_arcmin * 2 * np.pi/21600 #FHWM en radians
FWHM_correction = [np.sqrt(FWHM[0]**2 - f**2) for f in FWHM] #FHWM corrigee

N_BANDS = len(BANDS)
N_PIX = 12 * 512 ** 2
T = list()
images = [DATA_DIR + 'HFI_SkyMap_' + band + '_2048_R2.00_full.fits' for band in BANDS]
print(images)

for i, file_name in enumerate(images):
    # smooth and divide term by term to adapt units.
    # write to smooth* files
    a = hp.sphtfunc.smoothing(hp.read_map(file_name), FWHM_correction[i])
    a = a / UNITS[i] 
    hp.write_map(WORK_DIR + "data_2048/smooth_" + file_name.split('/')[-1], a)
   