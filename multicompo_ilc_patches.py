'''Applies the multiple component ILC method to the patches.'''

import numpy as np
from helpers import *

#Number of rejected components
N_rc = len(MC_spectrum[0]) - 1
e = np.array([1 if i == 0 else 0 for i in range(1 + N_rc)])
for i in range(N_CLUSTERS):
    folder = OUTPUT_DIR + dic['NAME'][i] + '/'
    images = [folder + band + '.fits' for band in BANDS]

    # same as ILC but with many spectra
    T = np.array([fits_to_array(image).flatten() for image in images])
    CT = np.cov(T)
    CT_inv = np.linalg.inv(CT)
    F = MC_spectrum
    tF = np.transpose(F)

    A = np.linalg.inv(np.dot(tF, np.dot(CT_inv, F)))
    B = np.dot(tF, np.dot(CT_inv, T))
    S_MCILC = np.dot(e, np.dot(A, B))
    MCILC_2D = np.array([[S_MCILC[256 * j + i] for i in range(256)] for j in range(256)])
    array_to_fits(MCILC_2D, folder + 'MCILC_tSZ.fits')
